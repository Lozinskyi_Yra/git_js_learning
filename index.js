const people = ["Olia", "Diana", "Tania"];
const one = 1;
const str = "String"
const b = true;
const person = {
    firstName: "Yrii",
    lastName: "Lozinskiy"
}

function sayHello(person) {
    console.log(`Hello ${person}`);
}

sayHello(typeof people);
sayHello(typeof one);
sayHello(typeof str);
sayHello(typeof b);
sayHello(typeof person);

try {
    throw "MyException"
} catch (e) {
    console.log("Exception is hear: " + e);
} finally {
    console.log("Finally section has worked");
}

const date = new Date();
console.log(date);
const status = 200;
const message = (status === 200) ? 'OK' : 'Error';
console.log(message)

let arrayLength = 5;
let arr1 = [];
let arr2 = Array(5);
arr1.push("hello");
console.log(arr1.length);
console.log(arr2.length);



